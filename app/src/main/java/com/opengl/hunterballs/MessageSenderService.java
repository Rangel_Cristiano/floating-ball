package com.opengl.hunterballs;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;

public class MessageSenderService extends Service {
    public static final String BROADCAST_EVENT = "broadcast-event";

    @Override public IBinder onBind(Intent intent) {
        return null;
    }

    @Override public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public void sendScore(int score) {
        Intent intent = new Intent(BROADCAST_EVENT);
        intent.putExtra("score", score);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void finish(int score) {
        Intent intent = new Intent(BROADCAST_EVENT);
        intent.putExtra("finished", score);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    public void restart() {
        Intent intent = new Intent(BROADCAST_EVENT);
        intent.putExtra("restart", "restart");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}