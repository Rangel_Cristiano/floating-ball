/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.opengl.hunterballs;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.app.AlertDialog;
import android.content.Context;
import android.media.MediaPlayer;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Random;
import java.util.Timer;
import java.util.concurrent.ConcurrentLinkedQueue;

public class GLRenderer implements GLSurfaceView.Renderer {
    /** Clear colour, alpha component. */
    private static final float CLEAR_RED = 0.0f;

    /** Clear colour, alpha component. */
    private static final float CLEAR_GREEN = 0.0f;

    /** Clear colour, alpha component. */
    private static final float CLEAR_BLUE = 0.0f;

    /** Clear colour, alpha component. */
    private static final float CLEAR_ALPHA = 0.5f;

    private WallSides mWallSides;
    private WallDown mWallDown;
    private WallUp mWallUp;
    private FinalTunnel mFinalTunnel;
    private Ball mBall;
    private ConcurrentLinkedQueue<Ball> mBalls;
    private Random sRandom;

    private float mAngle, mX, mY, a;
    private boolean mTranslucentBackground, mGameOver, mPaused;
    private Context mContext;
    private int mScore;
    private Timer mTimer;
    private MessageSenderService mMessageSenderService;

    public GLRenderer(Context context) {
        mBall = new Ball(50,50,0.50f, 1f, false);
        mTranslucentBackground = true;
        mWallSides = new WallSides();
        mWallUp = new WallUp();
        mFinalTunnel = new FinalTunnel();
        mBalls = new ConcurrentLinkedQueue<Ball>();
        mWallDown = new WallDown();
        mContext = context;
        mMessageSenderService = new MessageSenderService();
        sRandom = new Random();
    }

    @Override public void onSurfaceCreated(final GL10 gl, EGLConfig config) {
        gl.glDisable(GL10.GL_DITHER);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_FASTEST);
        if (mTranslucentBackground) {
            gl.glClearColor(0,0,0,0);
        } else {
            gl.glClearColor(1,1,1,1);
            gl.glEnable(GL10.GL_CULL_FACE);
            gl.glShadeModel(GL10.GL_SMOOTH);
            gl.glEnable(GL10.GL_DEPTH_TEST);
        }

        gl.glEnable(GL10.GL_TEXTURE_2D);
        gl.glShadeModel(GL10.GL_SMOOTH);
        gl.glClearColor(CLEAR_RED, CLEAR_GREEN, CLEAR_BLUE, CLEAR_ALPHA);
        gl.glClearDepthf(1.0f);
        gl.glEnable(GL10.GL_DEPTH_TEST);
        gl.glDepthFunc(GL10.GL_LEQUAL);
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST);
    }

    @Override public void onDrawFrame(final GL10 gl) {
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        gl.glMatrixMode(GL10.GL_MODELVIEW);
        gl.glLoadIdentity();
        gl.glTranslatef(0, 0, -15f);
        gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
        gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);

        mWallSides.draw(gl);
        mWallUp.draw(gl);
        mWallDown.draw(gl);
        mFinalTunnel.draw(gl);
        // mTransY += .075f;

        gl.glLoadIdentity();
        gl.glTranslatef(mX, mY, -3.0f);

        gl.glRotatef(mAngle, 1, 0, 0); //gira vertical
        //gl.glRotatef(mAngle, 0, 0, 1); //gira horizontal

        mBall.draw(gl);

        mAngle += 20;

        for (Ball ball : mBalls) {
            float depth = ball.getDepth();

            gl.glLoadIdentity();
            gl.glTranslatef(((float) ball.getExplodeX()), (float) ball.getExplodeY(), 20 - depth);
            gl.glRotatef(mAngle, -1, 0, 0);

            ball.setDepth(depth + 0.1f);

            ball.draw(gl);

            if (depth > 23 && depth < 24) {
                final double explodeX = ball.getExplodeX();
                final double explodeY = ball.getExplodeY();
                final double meX = mX;
                final double meY = mY;

                if (!mPaused && !mGameOver && Math.abs(explodeX - meX) <= 1f && Math.abs(explodeY - meY) <= 1f) {
                    mBalls.remove(ball);

                    if (ball.isKiller()) {
                        mGameOver = true;
                        mMessageSenderService.finish(mScore);
                        mBalls.clear();
                        mScore = 0;
                    } else {
                        mMessageSenderService.sendScore(mScore += 100);
                    }

                    try {
                        final MediaPlayer mediaPlayer = MediaPlayer.create(mContext.getApplicationContext(), R.raw.metal);
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                        mediaPlayer.start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } else if (depth >= 40) {
                mBalls.remove(ball);
            }
        }
    }

    @Override public void onSurfaceChanged(GL10 gl, int width, int height) {
        gl.glViewport(0, 0, width, height);
        float ratio = (float) width / height;
        gl.glMatrixMode(GL10.GL_PROJECTION);
        gl.glLoadIdentity();

        GLU.gluPerspective(gl, 28, ratio, 2f, 1000f);
        GLU.gluLookAt(gl, 0, 0, 10, 0, 0, 0, 0, 1,0);
    }

    public void setXY(float x, float y) {
        mX = x;
        mY = y;
    }

    public void createBall() {
        int x = sRandom.nextInt(100);
        final Ball ball = new Ball(50,50,0.50f, 1f, x < 50);

        ball.setExplodeX(random(-3.5, 3.5));
        ball.setExplodeY(random(-2, 2));

        mBalls.add(ball);
    }

    public void setTimer(Timer timer) {
        mTimer = timer;
    }

    public void setGameOver(boolean mGameOver) {
        this.mGameOver = mGameOver;
    }

    private static final Random generator = new Random();

    public static double random(double min, double max) {
        return min + (generator.nextDouble() * (max - min));
    }

    public boolean isPaused() {
        return mPaused;
    }

    public void setPaused(boolean mPaused) {
        this.mPaused = mPaused;
    }
}