/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.opengl.hunterballs;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.*;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Chronometer;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

import java.sql.Time;

import static com.opengl.hunterballs.MessageSenderService.BROADCAST_EVENT;

public class OpenGLActivity extends Activity {
    private CustomGLSurfaceView mGlSurfaceView;
    private SensorManager mSensorManager;
    private Chronometer mChronometer;
    private BroadcastReceiver mMessageReceiver;
    private TextView mScoreTextView;
    private MessageSenderService mMessageSender;
    private int mBestScore;
    private InterstitialAd mInterstitialAd;

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        // TODO: Move this to where you establish a user session
        logUser();

        final GLRenderer renderer = new GLRenderer(this);

        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.mGlSurfaceView = new CustomGLSurfaceView(this, renderer);
        this.mGlSurfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        this.mGlSurfaceView.setRenderer(renderer);
        this.mGlSurfaceView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); //FAZ ELE RENDERIZAR SÓ QUANDO CHAMA o requestRender

        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.setContentView(R.layout.main);
        startService(new Intent(OpenGLActivity.this, MessageSenderService.class));
        mMessageSender = new MessageSenderService();

        final SharedPreferences sharedPreferences = getSharedPreferences("saveTheBall", Context.MODE_PRIVATE);
        mBestScore = sharedPreferences.getInt("score", 0);
        ((TextView) findViewById(R.id.bestScore)).setText("Best score: " + mBestScore);

        mMessageReceiver = new BroadcastReceiver() {
            @Override public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("score")) {
                    final int score = intent.getIntExtra("score", 0);
                    final CharSequence text = " Score: " + score;
                    mScoreTextView.setText(text);
                } else if (intent.hasExtra("finished")) {
                    mChronometer.stop();
                    final Time time = getSeconds(mChronometer.getText().toString());
                    final int seconds = time.getSeconds();
                    final int bonus = seconds * 10;
                    final int score = intent.getIntExtra("finished", 0) + bonus;

                    if (score > mBestScore) {
                        final SharedPreferences sharedPreferences = getSharedPreferences("saveTheBall", Context.MODE_PRIVATE);
                        final SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putInt("score", score);
                        editor.apply();

                        mBestScore = score;

                        ((TextView) findViewById(R.id.bestScore)).setText("Best score: " + mBestScore);
                    }

                    final AlertDialog alertDialog = new AlertDialog.Builder(OpenGLActivity.this).create();
                    alertDialog.setTitle("GAME OVER");
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setMessage("Total time: " + mChronometer.getText() + "\n\nBonus points: " + bonus + "\n\nFinal Score: " + score);
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Play Again", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            if (mInterstitialAd.isLoaded()) {
                                mInterstitialAd.show();
                            } else {
                                Log.d("TAG", "The interstitial wasn't loaded yet.");

                                final Intent intent = getIntent();
                                finish();
                                startActivity(intent);
                            }
                        }
                    });

                    alertDialog.show();
                } else if (intent.hasExtra("restart")) {
                    mChronometer.setBase(SystemClock.elapsedRealtime());
                    mChronometer.start();
                    mScoreTextView.setText("Score: 0");
                    renderer.setGameOver(false);
                }
            }
        };

        mChronometer = (Chronometer) findViewById(R.id.chronometer);
        mChronometer.start();

        ((LinearLayout) this.findViewById(R.id.mainLinearLayout)).addView(this.mGlSurfaceView);
        mScoreTextView = (TextView) findViewById(R.id.scoreTextView);

        MobileAds.initialize(this,
                "ca-app-pub-2226174062692170~4665570995");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2226174062692170/1895937424");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();

                mInterstitialAd.loadAd(new AdRequest.Builder().build());

                final Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        Crashlytics.setUserIdentifier("12345");
        Crashlytics.setUserEmail("rangeltonin29@gmail.com");
        Crashlytics.setUserName("Test User");
    }


    private Time getSeconds(String timeString) {
        int firstIndex = timeString.indexOf(':');
        int minute = Integer.parseInt(timeString.substring(0, firstIndex));
        int second = Integer.parseInt(timeString.substring(firstIndex + 1, timeString.length()));
        return new Time(0, minute, second);
    }

    @Override protected void onPause() {
        this.mGlSurfaceView.onPause();
        mSensorManager.unregisterListener(mGlSurfaceView);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        mGlSurfaceView.setPaused(true);
        mChronometer.stop();
        super.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
        this.mGlSurfaceView.onResume();
        mGlSurfaceView.setPaused(false);
        mChronometer.start();
        mSensorManager.registerListener(mGlSurfaceView, mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_FASTEST);
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(BROADCAST_EVENT));
    }

    @Override protected void onStop() {
        mSensorManager.unregisterListener(mGlSurfaceView);
        super.onStop();
    }
}