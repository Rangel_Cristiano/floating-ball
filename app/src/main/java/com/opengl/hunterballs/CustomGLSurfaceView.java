package com.opengl.hunterballs;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.support.v4.content.LocalBroadcastManager;

import java.util.Timer;
import java.util.TimerTask;

import static com.opengl.hunterballs.MessageSenderService.BROADCAST_EVENT;

/**
 * A view container where OpenGL ES graphics can be drawn on screen.
 * This view can also be used to capture touch events, such as a user
 * interacting with drawn objects.
 */
public class CustomGLSurfaceView extends GLSurfaceView implements SensorEventListener {
    private final GLRenderer mRenderer;
    private BroadcastReceiver mMessageReceiver;
    private boolean mGameOver;

    public CustomGLSurfaceView(Context context, GLRenderer renderer) {
        super(context);

        mRenderer = renderer;
        mMessageReceiver = new BroadcastReceiver() {
            @Override public void onReceive(Context context, Intent intent) {
                if (intent.hasExtra("finished")) {
                    mGameOver = true;
                } else if (intent.hasExtra("restart")) {
                    mGameOver = false;
                }
            }
        };

        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver, new IntentFilter(BROADCAST_EVENT));

        final Timer timer = new Timer();
        mRenderer.setTimer(timer);

        postDelayed(new Runnable() {
            @Override public void run() {
                timer.schedule(new TimerTask() {
                    @Override public void run() {
                        if (!mGameOver && !mRenderer.isPaused()) {
                            mRenderer.createBall();
                            requestRender();
                        }
                    }
                }, 0, 500);
            }
        }, 200);
    }

    @Override public void onSensorChanged(SensorEvent event) {
        //if sensor is unreliable, return void
        if (event.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) {
            return;
        }

        float x = event.values[1] / 10 * -1f;
        float y = event.values[2] / 30 * -1f;

        if (x > 3.5f) x = 3.5f;
        if (x < -3.5f) x = -3.5f;
        if (y > 1.5f) y = 1.5f;
        if (y < -1.5f) y = -1.5f;

        mRenderer.setXY(x, y);
        requestRender();
    }

    @Override public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public void setPaused(boolean paused) {
        mRenderer.setPaused(paused);
    }
}